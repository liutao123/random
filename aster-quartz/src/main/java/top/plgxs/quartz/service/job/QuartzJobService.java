package top.plgxs.quartz.service.job;

import top.plgxs.quartz.entity.job.QuartzJob;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 定时任务 服务类
 * </p>
 *
 * @author Stranger.
 * @since 2021-08-10
 * @version 1.0
 */
public interface QuartzJobService extends IService<QuartzJob> {
    /**
     * 数据查询列表
     * @return
     * @author Stranger.
     * @date 2021-08-10
     */
    List<QuartzJob> getQuartzJobList();
}
