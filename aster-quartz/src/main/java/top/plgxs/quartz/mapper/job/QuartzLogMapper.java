package top.plgxs.quartz.mapper.job;

import top.plgxs.quartz.entity.job.QuartzLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 定时任务日志 Mapper 接口
 * </p>
 *
 * @author Stranger.
 * @since 2021-08-10
 * @version 1.0
 */
public interface QuartzLogMapper extends BaseMapper<QuartzLog> {

    /**
     * 分页数据查询
     * @param quartzLog
     * @return
     * @author Stranger.
     * @date 2021-08-10
     */
    List<QuartzLog> selectQuartzLogList(QuartzLog quartzLog);
}
