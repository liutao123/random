package top.plgxs.admin.service.vacc;

import top.plgxs.mbg.entity.vacc.ChildInfo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Stranger.
 * @since 2021-08-02
 * @version 1.0
 */
public interface ChildInfoService extends IService<ChildInfo> {
    /**
     * 数据查询列表
     * @return
     * @author Stranger.
     * @date 2021-08-02
     */
    List<ChildInfo> getChildInfoList();

    /**
     * 插入
     * @param childInfo
     * @return void
     * @author Stranger。
     * @since 2021/8/3
     */
    void insert(ChildInfo childInfo);

    /**
     * 测试master主库
     * @return void
     * @author Stranger。
     * @since 2021/11/5
     */
    void selectMaster();

    /**
     * 测试slave从库
     * @return void
     * @author Stranger。
     * @since 2021/11/5
     */
    void selectSlave();

    /**
     * 测试sharding-jdbc分库分表
     * @return void
     * @author Stranger。
     * @since 2021/11/5
     */
    void insertSharding();

    /**
     * 测试分库分表
     * @return java.util.List<top.plgxs.mbg.entity.vacc.ChildInfo>
     * @author Stranger。
     * @since 2021/11/5
     */
    List<ChildInfo> selectSharding();
}
