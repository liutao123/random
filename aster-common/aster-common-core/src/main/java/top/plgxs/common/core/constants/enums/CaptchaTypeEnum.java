package top.plgxs.common.core.constants.enums;

/**
 * 验证码展现方式
 * @author Stranger。
 * @version 1.0
 * @since 2021/11/5 14:34
 */
public enum CaptchaTypeEnum {
    /**
     * 加减乘除
     */
    MATH("math", "计算"),
    /**
     * 字符
     */
    CHAR("char", "字符");

    private String code;
    private String message;

    CaptchaTypeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
